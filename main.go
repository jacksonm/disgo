package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jacksonm/disgo/handler/web"
	"gitlab.com/jacksonm/disgo/internal/cluster"
	"gitlab.com/jacksonm/disgo/pkg/logger"
	"net/http"
	"time"
)

func main() {
	l := logger.New("info")

	options := cluster.Options{
		StartNodes:        []string{"127.0.0.1:7000", "127.0.0.1:7001", "127.0.0.1:7002"},
		ConnectionTimeout: 1 * time.Hour,
		KeepAliveTime:     1 * time.Hour,
		Pool:              2,
	}

	c, err := cluster.New(&options)
	if err != nil {
		panic(err)
	}

	handler := gin.Default()
	web.NewRouter(handler, l, c)
	httpServer := &http.Server{
		Addr:    ":1234",
		Handler: handler,
	}
	_ = httpServer.ListenAndServe()
}
