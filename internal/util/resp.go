package util

import (
	"errors"
	"fmt"
	"strconv"
)

var ErrNil = errors.New("nil resp")

func Bytes(resp interface{}, err error) ([]byte, error) {
	if err != nil {
		return nil, err
	}
	switch resp := resp.(type) {
	case []byte:
		return resp, nil
	case string:
		return []byte(resp), nil
	case nil:
		return nil, ErrNil
	}
	return nil, fmt.Errorf("unexpected type %T for Bytes", resp)
}

func Int(resp interface{}, err error) (int, error) {
	if err != nil {
		return 0, err
	}
	switch resp := resp.(type) {
	case int:
		return resp, nil
	case []byte:
		n, err := strconv.ParseInt(string(resp), 10, 0)
		return int(n), err
	case string:
		return strconv.Atoi(resp)
	case nil:
		return 0, ErrNil
	}
	return 0, fmt.Errorf("unexpected type %T for Int", resp)
}

func String(resp interface{}, err error) (string, error) {
	if err != nil {
		return "", err
	}
	switch resp := resp.(type) {
	case []byte:
		return string(resp), nil
	case string:
		return resp, nil
	case nil:
		return "", ErrNil
	}
	return "", fmt.Errorf("unexpected type %T for String", resp)
}

func Values(resp interface{}, err error) ([]interface{}, error) {
	if err != nil {
		return nil, err
	}
	switch resp := resp.(type) {
	case []interface{}:
		return resp, nil
	case nil:
		return nil, ErrNil
	}
	return nil, fmt.Errorf("unexpected type %T for Values", resp)
}
