package client

import (
	"encoding/json"
	"fmt"
	u "gitlab.com/jacksonm/disgo/internal/util"
	"gitlab.com/jacksonm/disgo/pkg/redis"
	"strings"
)

type Client struct {
	rdb *redis.RdbClient
}

func New(addr string) *Client {
	return &Client{rdb: redis.New(addr)}
}

func (c Client) Execute(cmd string, args ...interface{}) (interface{}, error) {
	switch cmd {
	case "CLUSTER":
		return c.ClusterNodes(args...)
	case "GET":
		return c.Get(args...)
	case "SET":
		return c.Set(args...)
	default:
		return nil, fmt.Errorf("client - Execute - invalid command: %v", cmd)
	}
}

func (c Client) Get(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return nil, fmt.Errorf("client - Get - Insufficent arguments length")
	}

	key, err := u.String(args[0], nil)
	if err != nil {
		return nil, fmt.Errorf("client - Get - u.String(): %w", err)
	}

	result, err := c.rdb.Redis.Get(key).Result()
	if err != nil {
		return nil, fmt.Errorf("client - Get - c.rdb.Redis.Get(): %w", err)
	}

	r, err := u.Bytes(result, err)
	if err != nil {
		return nil, fmt.Errorf("client - Get - u.Bytes(): %w", err)
	}

	var dest = make(map[string]interface{})
	err = json.Unmarshal(r, &dest)
	if err != nil {
		return nil, fmt.Errorf("client - Get - json.Unmarshal(): %w", err)
	}

	return dest, nil
}

func (c Client) Set(args ...interface{}) (interface{}, error) {
	if len(args) < 2 {
		return nil, fmt.Errorf("client - Set - Insufficent arguments length")
	}

	key, err := u.String(args[0], nil)
	if err != nil {
		return nil, fmt.Errorf("client - Get - u.String(): %w", err)
	}

	value, err := u.Bytes(args[1], nil)
	if err != nil {
		return nil, fmt.Errorf("client - Get - u.String(): %w", err)
	}

	err = c.rdb.Redis.Set(key, value, c.rdb.CacheDuration).Err()
	if err != nil {
		return nil, fmt.Errorf("client - Set - c.rdb.Redis.Set(): %w", err)
	}

	return nil, nil
}

func (c Client) ClusterNodes(args ...interface{}) (interface{}, error) {
	resp, err := c.rdb.Redis.ClusterNodes().Result()
	if err != nil {
		return nil, fmt.Errorf("client - CusterNodes - c.rdb.Redis.ClusterNodes(): %w", err)
	}

	slots, err := c.slotsForNode(resp)
	if err != nil {
		return nil, fmt.Errorf("client - CusterNodes - c.slotsForNode(): %w", err)
	}

	return slots, nil
}

func (c Client) Close() interface{} {
	c.rdb.Close()
	return nil
}

func (c Client) slotsForNode(resp string) (interface{}, error) {
	var err error
	var slotsRange string
	clusterSlots := strings.Split(resp, "\n")
	for x := range clusterSlots {
		if strings.Contains(clusterSlots[x], "myself") {
			slotsRange = clusterSlots[x]
		}
	}

	if slotsRange == "" {
		err = fmt.Errorf("cluster - slotsForNode - no node with matching address")
	}

	ports := strings.Split(slotsRange, " ")
	slots := strings.Split(ports[len(ports)-1], "-")

	r := make([]interface{}, len(slots))
	for i := range r {
		l, err := u.Int(slots[i], err)
		if err != nil {
			return nil, fmt.Errorf("cluster - slotsForNode - unable to find valid slot range for node")
		}
		r[i] = l
	}

	return r, nil
}
