package node

import (
	"container/list"
	"fmt"
	c "gitlab.com/jacksonm/disgo/internal/node/client"
	"sync"
	t "time"
)

type Node struct {
	Addr        string
	connections list.List

	Pool              int
	ConnectionTimeout t.Duration
	KeepAliveTime     t.Duration

	mutex  sync.Mutex
	closed bool
}

func NewNode(addr string, p int, timeout t.Duration, kal t.Duration) *Node {
	return &Node{
		Addr:              addr,
		connections:       list.List{},
		Pool:              p,
		ConnectionTimeout: timeout,
		KeepAliveTime:     kal,
	}
}

func (node *Node) getConn() (*Connection, error) {
	node.mutex.Lock()

	if node.closed {
		node.mutex.Unlock()
		return nil, fmt.Errorf("node - getConn - connection has been closed")
	}

	if node.ConnectionTimeout > 0 {
		for {
			cn := node.connections.Back()
			if cn == nil {
				break
			}
			conn := cn.Value.(*Connection)
			if conn.t.Add(node.KeepAliveTime).After(t.Now()) {
				break
			}
			node.connections.Remove(cn)
		}
	}

	if node.connections.Len() <= 0 {
		node.mutex.Unlock()

		conn := New(c.New(node.Addr), t.Now())
		return conn, nil
	}

	conn := node.connections.Back()
	node.connections.Remove(conn)

	node.mutex.Unlock()
	return conn.Value.(*Connection), nil
}

func (node *Node) releaseConn(conn *Connection) {
	node.mutex.Lock()
	defer node.mutex.Unlock()

	if node.closed {
		conn.shutdown()
		return
	}

	if node.connections.Len() >= node.Pool || node.KeepAliveTime <= 0 {
		conn.shutdown()
		return
	}

	conn.t = t.Now()
	node.connections.PushFront(conn)
}

func (node *Node) Shutdown() {
	node.mutex.Lock()
	defer node.mutex.Unlock()

	for {
		cn := node.connections.Back()
		if cn == nil {
			break
		}
		conn := cn.Value.(*Connection)
		_ = conn.c.Close()
		node.connections.Remove(cn)
	}

	node.closed = true
}

// cluster.Do("SET", "foo", "bar")
// cluster.Do("GET", "foo")
func (node *Node) Do(cmd string, args ...interface{}) (interface{}, error) {
	conn, err := node.getConn()
	if err != nil {
		return nil, fmt.Errorf("node - Do - connection to node timed out: %q", node.Addr)
	}

	resp, err := conn.send(cmd, args...)
	if err != nil {
		conn.shutdown()
		return nil, err
	}

	node.releaseConn(conn)
	return resp, err
}
