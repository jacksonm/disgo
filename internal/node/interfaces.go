package node

type IClient interface {
	Execute(cmd string, args ...interface{}) (interface{}, error)

	Get(args ...interface{}) (interface{}, error)
	Set(args ...interface{}) (interface{}, error)
	ClusterNodes(args ...interface{}) (interface{}, error)

	Close() interface{}
}
