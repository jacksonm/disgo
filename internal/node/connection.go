package node

import (
	t "time"
)

type Connection struct {
	c IClient
	t t.Time
}

func New(c IClient, t t.Time) *Connection {
	return &Connection{
		c: c,
		t: t,
	}
}

// cluster.Do("SET", "foo", "bar")
// cluster.Do("GET", "foo")
func (conn *Connection) send(cmd string, args ...interface{}) (interface{}, error) {

	resp, err := conn.c.Execute(cmd, args...)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (conn *Connection) shutdown() {
	_ = conn.c.Close()
}
