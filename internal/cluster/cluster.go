package cluster

import (
	"fmt"
	n "gitlab.com/jacksonm/disgo/internal/node"
	u "gitlab.com/jacksonm/disgo/internal/util"
	"log"
	"strconv"
	"sync"
	"time"
)

const kClusterSlots = 16384

type Options struct {
	StartNodes []string

	ConnectionTimeout time.Duration
	KeepAliveTime     time.Duration
	Pool              int
}

type Cluster struct {
	Slots [kClusterSlots]*n.Node
	Nodes map[string]*n.Node

	connectionTimeout time.Duration
	keepAliveTime     time.Duration
	pool              int

	mutex  sync.Mutex
	closed bool
}

func New(options *Options) (*Cluster, error) {
	cluster := &Cluster{
		Nodes:             make(map[string]*n.Node),
		connectionTimeout: options.ConnectionTimeout,
		keepAliveTime:     options.KeepAliveTime,
		pool:              options.Pool,
	}

	for i := range options.StartNodes {
		node := n.NewNode(options.StartNodes[i], options.Pool, options.ConnectionTimeout, options.KeepAliveTime)

		err := cluster.update(node)
		if err != nil {
			return nil, err
		}
	}

	return cluster, nil
}

// cluster.Do("SET", "foo", "bar")
// cluster.Do("GET", "foo")
func (cluster *Cluster) Do(cmd string, args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return nil, fmt.Errorf("cluster - do - no key found in args")
	}

	node, err := cluster.getNodeByKey(args[0])
	if err != nil {
		return nil, fmt.Errorf("cluster - getNodeByKey - %v", err)
	}

	resp, err := node.Do(cmd, args...)
	if err != nil {
		return nil, fmt.Errorf("cluster - Do - %v", err)
	}

	return resp, nil
}

func (cluster *Cluster) Close() {
	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()

	for addr, node := range cluster.Nodes {
		node.Shutdown()
		delete(cluster.Nodes, addr)
	}

	cluster.closed = true
}

func (cluster *Cluster) update(node *n.Node) error {
	resp, err := u.Values(node.Do("CLUSTER", "SLOTS"))
	if err != nil {
		return fmt.Errorf("cluster - update - node.Do(): invalid response for node %s", node.Addr)
	}

	start, err := u.Int(resp[0], err)
	if err != nil {
		return fmt.Errorf("cluster - update - node.Do(): invalid start slot range for node %s", node.Addr)
	}

	end, err := u.Int(resp[1], err)
	if err != nil {
		return fmt.Errorf("cluster - update - node.Do(): invalid end slot range for node %s", node.Addr)
	}

	for i := start; i <= end; i += 1 {
		cluster.Slots[i] = node
	}

	cluster.Nodes[node.Addr] = node
	return nil
}

func (cluster *Cluster) getNodeByKey(arg interface{}) (*n.Node, error) {
	key, err := key(arg)
	if err != nil {
		return nil, fmt.Errorf("cluster - getNodeByKey - invalid key %v", key)
	}

	slot := hash(key)
	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()

	if cluster.closed {
		return nil, fmt.Errorf("cluster - getNodeByKey - cluster has been closed")
	}

	node := cluster.Slots[slot]
	if node == nil {
		return nil, fmt.Errorf("cluster - getNodeByKey - %s[%d] no node found", key, slot)
	}

	return node, nil
}

// key Transforms key into string representation
func key(arg interface{}) (string, error) {
	switch arg := arg.(type) {
	case int:
		return strconv.Itoa(arg), nil
	case int64:
		return strconv.Itoa(int(arg)), nil
	case float64:
		return strconv.FormatFloat(arg, 'g', -1, 64), nil
	case string:
		return arg, nil
	case []byte:
		return string(arg), nil
	default:
		return "", fmt.Errorf("cluster - key - unknown type %T", arg)
	}
}

func hash(key string) uint16 {
	var s, e int
	for s = 0; s < len(key); s++ {
		if key[s] == '{' {
			break
		}
	}

	if s == len(key) {
		return u.Crc16(key) & (kClusterSlots - 1)
	}

	for e = s + 1; e < len(key); e++ {
		if key[e] == '}' {
			break
		}
	}

	if e == len(key) || e == s+1 {
		return u.Crc16(key) & (kClusterSlots - 1)
	}
	return u.Crc16(key[s+1:e]) & (kClusterSlots - 1)
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}
