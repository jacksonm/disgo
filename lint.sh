#!/bin/bash

set -e

go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
staticcheck $(go list ./... | grep -v /vendor/)
