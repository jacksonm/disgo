package redis

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

const (
	_DefaultCacheDuration = 6 * time.Hour
	_DefaultDatabase      = 0
)

type RdbClient struct {
	CacheDuration time.Duration
	dataBaseName  int

	Redis *redis.Client
}

func New(addr string) *RdbClient {
	rdb := &RdbClient{
		CacheDuration: _DefaultCacheDuration,
		dataBaseName:  _DefaultDatabase,
	}

	rdb.Redis = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "",
		DB:       _DefaultDatabase,
	})

	_, err := rdb.Redis.Ping().Result()
	if err != nil {
		panic(fmt.Sprintf("redis - New - Error init Redis: %v", err))
	}
	fmt.Printf("redis - Pong - %v\n", addr)

	return rdb
}

func (rdb *RdbClient) Close() {
	_ = rdb.Redis.Close()
}
