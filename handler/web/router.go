package web

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/jacksonm/disgo/internal/cluster"
	"gitlab.com/jacksonm/disgo/pkg/logger"
	"io/ioutil"
	"net/http"
)

type router struct {
	cluster *cluster.Cluster
	logger  logger.ILogger
}

func NewRouter(handler *gin.Engine, logger logger.ILogger, cluster *cluster.Cluster) {
	r := &router{cluster: cluster, logger: logger}
	handler.Use(gin.Recovery())

	h := handler.Group("/")
	{
		h.GET("get/:key", r.get)
		h.GET("set/:key", r.set)
	}
}

func (r *router) get(c *gin.Context) {
	key := c.Param("key")
	resp, err := r.cluster.Do("GET", key)

	if err != nil {
		r.logger.Error("http - v1 - get")
		errorResponse(c, http.StatusNotFound, fmt.Sprintf("no saved object with key: %s", key))
		return
	}

	c.JSON(http.StatusOK, resp)
}

func (r *router) set(c *gin.Context) {
	key := c.Param("key")
	obj, err := ioutil.ReadAll(c.Request.Body)

	if err != nil {
		r.logger.Error(err, "http - v1 - set")
		errorResponse(c, http.StatusBadRequest, "invalid request body")
		return
	}

	resp, err := r.cluster.Do("SET", key, obj)
	if err != nil {
		r.logger.Error(err, "http - v1 - set")
		errorResponse(c, http.StatusInternalServerError, fmt.Sprintf("unable to save object. key: %s object: %s", key, obj))
		return
	}

	c.JSON(http.StatusOK, resp)
}

type response struct {
	Error string `json:"error" example:"message"`
}

func errorResponse(c *gin.Context, code int, msg string) {
	c.AbortWithStatusJSON(code, response{msg})
}
