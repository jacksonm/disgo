package config

type Shard struct {
	Name string
	Idx  int
	Addr string
}

type Config struct {
	Shards []Shard
}
